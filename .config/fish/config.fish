source "$HOME/.config/fish/setup_env.fish"
set -a fish_user_paths /opt/homebrew/bin
set -p fish_user_paths $XDG_DATA_HOME/cargo/bin

alias eza "eza --icons"
alias ls "eza --icons"

fzf --fish | source
zoxide init fish | source
starship init fish | source
