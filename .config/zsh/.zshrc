autoload -Uz vcs_info
precmd() { vcs_info }

zstyle ':vcs_info:git:*' formats '%b '

setopt PROMPT_SUBST
PROMPT='%F{green}%*%f %F{blue}%~%f %F{red}${vcs_info_msg_0_}%f$ '

source ~/.local/share/zsh/zsh-autosuggestions/zsh-autosuggestions.zsh

source ~/.local/share/zsh/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
