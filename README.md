# WeetHet's dotfiles

To bootstrap, start with installing yadm:
```sh
curl -fLo ~/.local/bin/yadm https://github.com/TheLocehiliosan/yadm/raw/master/yadm 
chmod a+x ~/.local/bin/yadm
```

Get this repo:
```sh
~/.local/bin/yadm clone https://gitlab.com/WeetHet/yadm-dotfiles.git
```

Bootstrap via `~/.local/share/bin/yadm bootstrap`

Import iTerm profiles from `~/.config/unmanaged/Profiles.json`

Enjoy!
